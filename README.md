# Infrastructure

Agrupamento de tarefas para deploy, migração, criação de containers, etc.

## Estrutura de pastas

- **docker**: Scripts para Docker.
  - **compose**: Scripts para orquestrar containers.
  - **images**: Scripts para criação de imagens.

Veja em cada pasta o README.
